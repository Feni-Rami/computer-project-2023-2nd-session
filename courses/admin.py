from django.contrib import admin
from .models import Course, Copy

@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher', 'list_followed_students')
    list_filter = ('teacher',)
    search_fields = ('name', 'teacher__username')
    filter_horizontal = ('students',)

    def list_followed_students(self, obj):
        return ", ".join([student.username for student in obj.students.all()])

    list_followed_students.short_description = 'Followed Students'

@admin.register(Copy)
class CopyAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'graded','mark_student','filename','file')
    list_filter = ('course', 'graded')
    search_fields = ('student__username', 'course__name')
