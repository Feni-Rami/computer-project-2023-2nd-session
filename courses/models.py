from django.db import models
from accounts.models import CustomUser as User

class Course(models.Model):
    name = models.CharField(max_length=100)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, related_name='courses_taught')
    students = models.ManyToManyField(User, related_name='courses_taken', blank=True)

    def __str__(self):
        return self.name
    
class Copy(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    graded = models.BooleanField(default=False)
    mark = models.CharField(null=True, max_length=2)
    mark_student = models.BinaryField(null=True, max_length=2)
    filename = models.BinaryField(null=True)
    file = models.FileField(upload_to='server_list/')

    def __str__(self):
        return f"Copy for {self.student} - Course: {self.course}"
