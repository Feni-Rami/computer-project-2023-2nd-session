# courses/urls.py

from django.urls import path
from . import views

app_name = 'courses'

urlpatterns = [
    path('courses/', views.courses_list, name='courses_list'),
    path('permission/', views.permission, name='permission'),
    path('logout/', views.logout_view, name='logout'),
    path('dashboard/teacher/', views.teacher_dashboard, name='teacher_dashboard'),
    path('dashboard/admin/', views.admin_dashboard, name='admin_dashboard'),
    path('dashboard/student/', views.student_dashboard, name='student_dashboard'),
    path('delete/<int:course_id>/', views.delete_course, name='delete_course'),
    path('create/', views.create_course, name='create_course'),
    path('copy/', views.view_copy, name='view_copy'),
    path('download/',views.download_encrypted_file, name='download_encrypted_file')
]
