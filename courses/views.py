# courses/views.py

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseForbidden
from django.core.files.base import ContentFile
from .forms import CourseForm, CopyForm
from .models import Copy, Course
from accounts.models import CustomUser
from utils.encryption_utils import decrypt_file, decrypt_with_private_key, encrypt_file, encrypt_with_public_key, generate_key
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from django.utils.encoding import smart_str

@login_required(login_url="login")
def logout_view(request):
    logout(request)
    return redirect('home')

@login_required(login_url="login")
def courses_list(request):
    student = request.user
    courses = student.courses_taken.all()
    return render(request, 'courses/list-courses.html', {'courses': courses})

@login_required(login_url="login")
def download_encrypted_file(request):
    copies = Copy.objects.filter(course__teacher=request.user)
    
    # Initialize with default values
    decrypted_file_content = 'error to download the file'
    decrypted_file_name = 'error_filename.txt'
    
    for copy in copies:
        if copy.file:  # Check if there's a file associated with the copy
            title = str(copy.file)
            encryption_key = generate_key(request.user.username)  # Get the encryption key
            decrypted_file_name = decrypt_file(copy.filename, encryption_key)  # Decrypt the filename
            try:
                with open(title, "rb") as file:
                    binary_data = file.read()
                decrypted_file_content = decrypt_file(binary_data, encryption_key)  # Decrypt the content
            except FileNotFoundError:
                print(f"File not found at path: {title}")
            except Exception as e:
                print(f"An error occurred: {str(e)}")

    # Create an HttpResponse object
    response = HttpResponse(content_type='application/octet-stream')
    
    # Use smart_str to handle non-ASCII characters in the filename
    response['Content-Disposition'] = f'attachment; filename="{smart_str(decrypted_file_name)}"'
    
    # Write the content to the response
    response.write(decrypted_file_content)    
    return response


@login_required(login_url="login")
def teacher_dashboard(request):
    user = request.user
    if user.user_type != 'teacher':
        return HttpResponseForbidden("You don't have permission to access this page.")

    if request.method == 'POST':
        form = CopyForm(user,request.POST, request.FILES)
        if form.is_valid():
            copy = form.save(commit=False)  # Save the form data without committing to the database
            copy.teacher = user
            if copy.file:  # Check if a file is provided
                encryption_key = generate_key(request.user.username)
                encrypted_data = encrypt_file(copy.file.read(), encryption_key)
                encrypted_filename = encrypt_file(str(copy.file).encode(), encryption_key)
                copy.filename = encrypted_filename
                copy.file.save(str(encrypted_filename), ContentFile(encrypted_data), save=False)
                
                mark = form.cleaned_data.get('mark')
                username = form.cleaned_data.get('student')
                student = CustomUser.objects.get(username=username)
                if mark is not None:
                    pubkey = serialization.load_pem_public_key(student.public_key, backend=default_backend())
                    encrypted_mark_student = encrypt_with_public_key(public_key=pubkey, data=str(mark))
                    encrypted_mark = encrypt_file(str(mark).encode(), encryption_key)
                    copy.mark = encrypted_mark
                    copy.mark_student = encrypted_mark_student
            
            copy.save()
            return redirect('courses:view_copy')
    else:
        form = CopyForm(user)
    
    courses_taught = user.courses_taught.all()  # Get courses taught by the teacher
    students = CustomUser.objects.filter(courses_taken__in=courses_taught).distinct()  # Filter students who took those courses
    courses = courses_taught
    
    context = {
        'form': form,
        'courses': courses,
        'students': students,
    }
    
    return render(request, 'courses/teacher_dashboard.html', context)

@login_required(login_url="login")
def view_copy(request):
    copies = Copy.objects.filter(course__teacher=request.user)
    result = []
    for copy in copies:
        if copy.file:  # Check if there's a file associated with the copy
            student = copy.student
            course = copy.course
            encryption_key = generate_key(request.user.username)  # Get the encryption key
            decrypted_file_name = decrypt_file(copy.filename, encryption_key)  # Decrypt the content
            result.append(('Student -> '+student.get_username(),' Course -> '+course.name, 'File -> '+decrypted_file_name.decode()))
    context = {
        'decrypted_content':result
    }
    return render(request, 'courses/view-copy.html',context)

@login_required(login_url="login")
def admin_dashboard(request):
    user = request.user

    if user.user_type != 'admin':
        return HttpResponseForbidden("You don't have permission to access this page.")
    
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('courses:admin_dashboard')
    else:
        form = CourseForm()

    courses = Course.objects.all()
    return render(request, 'courses/admin_dashboard.html', {'form': form, 'courses': courses})

@login_required(login_url="login")
def student_dashboard(request):
    user = request.user
    if user.user_type != 'student':
        return HttpResponseForbidden("You don't have permission to access this page.")
    return render(request, 'courses/student_dashboard.html')

@login_required(login_url="login")
def create_course(request): 
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('admin_dashboard')  # Redirect to admin dashboard after creating a course
    else:
        form = CourseForm()
    return render(request, 'courses/create-course.html', {'form': form})

@login_required(login_url="login")
def view_marks(request):
    return render(request, 'courses/view-marks.html')

@login_required(login_url="login")
def permission(request):
    if request.method == 'POST':
        private_key_text = request.POST.get('private_key')  # Get the private key from the form
        if private_key_text:
            private_key = serialization.load_pem_private_key(
                private_key_text.encode(),
                password=None
            )

            graded_copies = Copy.objects.filter(graded=True, mark_student__isnull=False)
            copy_data = []
            for copy in graded_copies:
                encrypted_mark = copy.mark_student
                decrypted_mark = decrypt_with_private_key(private_key, encrypted_mark)
                copy_info = {
                    'course_name': copy.course.name,
                    'mark_student': decrypted_mark.decode(),
                }
                copy_data.append(copy_info)

            context = {
                'copy_data': copy_data,
            }
            return render(request, 'courses/view-marks.html', context)

    return render(request, 'courses/permission.html')

@login_required(login_url="login")
def delete_course(request, course_id):
    course = get_object_or_404(Course, id=course_id)
    if request.method == 'POST':
        course.delete()
        return redirect('courses:admin_dashboard')
    return render(request, 'courses/delete-course.html', {'course': course})

