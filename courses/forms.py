from django import forms
from .models import Course, Copy
from accounts.models import CustomUser as User

class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'teacher', 'students']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Filter users by user type for students and teachers
        self.fields['teacher'].queryset = User.objects.filter(user_type='teacher')
        self.fields['students'].queryset = User.objects.filter(user_type='student')

class CopyForm(forms.ModelForm):
    def __init__(self, teacher, *args, **kwargs):
        super().__init__(*args, **kwargs)
        courses_taught = teacher.courses_taught.all()
        self.fields['course'].queryset = courses_taught
        self.fields['student'].queryset = User.objects.filter(courses_taken__in=courses_taught).distinct()

    class Meta:
        model = Copy
        fields = ['student', 'course', 'graded', 'mark', 'file']