# accounts/views.py

from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import RegistrationForm, LoginForm
from axes.utils import reset
from utils.encryption_utils import generate_rsa_keypair
from cryptography.hazmat.primitives import serialization

def home(request):
    return render(request, 'accounts/home.html')

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.user_type = form.cleaned_data['user_type']
            
            if user.user_type == 'student':
                if not user.public_key:  # Check if public key already exists for the user
                    public_key = generate_rsa_keypair(user.username) # the private key is already stored
                    store_public_key(user, public_key)
            user.save()
            
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            
            # Create a request object for authentication
            request_ = HttpRequest()
            request_.META['REMOTE_ADDR'] = '127.0.0.1'  # Replace with the appropriate IP address
            
            user = authenticate(request=request_, username=username, password=raw_password)
            
            if user is not None:
                login(request, user)
                # Reset the failed login attempts for this user
                reset(username=username)
                
                return redirect('home')  # Redirect to the home page after successful registration
    else:
        form = RegistrationForm()
    return render(request, 'accounts/register.html', {'form': form})

def store_public_key(user, public_key):
    encoded_public_key = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    user.public_key = encoded_public_key
    user.save()

def user_login(request):
    next = request.GET.get('next', '')  # Get the 'next' parameter from the query string
    if request.method == 'POST':
        form = LoginForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                if user.user_type == 'teacher':
                    return redirect('courses:teacher_dashboard')
                elif user.user_type == 'admin':
                    return redirect('courses:admin_dashboard')
                elif user.user_type == 'student':
                    return redirect('courses:student_dashboard')
                else:
                    return redirect('home')  # Default redirect
            else:
                return render(request, 'accounts/login.html', {'error_message': 'Invalid login credentials', 'next': next})

    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form, 'next': next})