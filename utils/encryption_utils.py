# utils/encryption_utils.py
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import padding
import os

def generate_rsa_keypair(name):
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )

    parentDirectory = '../../../../' #desktop
    directory = 'private/Student/'+name
    path = os.path.join(parentDirectory, directory)
    mode = 0o777

    # Create the directory and any missing parent directories
    os.makedirs(path, mode, exist_ok=True)

    # Serialize and save private key to PEM file
    with open(os.path.join(path, 'private_key.pem'), 'wb') as private_key_file:
        private_key_file.write(
            private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
            )
        )
    public_key = private_key.public_key()
    return public_key


def encrypt_with_public_key(public_key, data):
    if not public_key:
        raise ValueError("Public key is required for encryption")
    
    encrypted_data = public_key.encrypt(
        data.encode('utf-8'),
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    return encrypted_data

def decrypt_with_private_key(private_key, encrypted_data):
    decrypted_data = private_key.decrypt(
        encrypted_data,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    return decrypted_data

# Fernet for file
def generate_key(name):
    parentDirectory = '../../../../'  # Desktop: Change this to the appropriate parent directory
    directory = 'private/Teacher/' + name
    path = os.path.join(parentDirectory, directory)
    mode = 0o777

    # Create the directory and any missing parent directories
    os.makedirs(path, mode, exist_ok=True)
    
    key_path = os.path.join(path, name + '.key')
    
    if os.path.exists(key_path):
        # Read the existing key from the file
        with open(key_path, 'rb') as key_file:
            encryption_key = key_file.read()
    else:
        # Generate a new key and save it to the file
        encryption_key = Fernet.generate_key()
        with open(key_path, 'wb') as key_file:
            key_file.write(encryption_key)
    return encryption_key

def encrypt_file(file_content, key):
    fernet = Fernet(key)
    encrypted_file = fernet.encrypt(file_content)
    return encrypted_file

def decrypt_file(encrypted_content, key):
    fernet = Fernet(key)
    decrypted_content = fernet.decrypt(encrypted_content)
    return decrypted_content