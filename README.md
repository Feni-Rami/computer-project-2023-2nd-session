# Computer-Project-2023-2nd session
## Author
- 551855 __Andriamahefa Ramiaramanana__	
- 557692 __EYAGA NDEME	Solange Dominique__

## Create Certificat
- ## Generate a Private Key :
    
        > openssl genpkey -algorithm RSA -out private_key.pem
        > openssl req -new -key private_key.pem -out csr.pem
        > openssl x509 -req -days 365 -in csr.pem -signkey private_key.pem -out certificate.pem

## Installation
Before Running the project, these command need to be executed:

        >   sudo apt install python3-django
        >   sudo apt-get install openssl
        >   pip install django-sslserver
        >   pip install django-axes
        >   pip install django-extensions
        >   pip install cryptography
        >   pip install pyOpenSSL


## Compilation and Running
    > python3 manage.py runsslserver 8522 --cert cert.pem --key cert-key.pem

## Create admin-django:
    > python3 manage.py createsuperuser
    and follow the steps

## admin-django page web:
    > https://127.0.0.1:8522/admin
    And you can connect to the admin to see all the data in the database

## if you want to use the current the PEM pass phrase is: 090899Fe))
